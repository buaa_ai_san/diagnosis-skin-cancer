# 训练代码
from torchvision import datasets, transforms
import torch
from torch import nn
import torch.optim as optim
import argparse
import warnings
import torch.optim.lr_scheduler as lr_scheduler
from torch.utils.data.dataloader import default_collate  
from efficientnet_pytorch import EfficientNet #EfficientNet的使用需要倒入的库
import os

#GPU的使用配置
gpu_info = !nvidia-smi
gpu_info = '\n'.join(gpu_info)
if gpu_info.find('failed') >= 0:
  print('选择 "修改" → "笔记本设置" 切换到 GPU 硬件加速器, ')
  print('然后再重新执行.')
else:
  print(gpu_info) # 打印当前 GPU 信息 # 可以通过反复断开连接随机到更好的 GPU

#设置当前使用的GPU设备仅为0号设备  设备名称为'/gpu:0' 理论上是设置gpu可见，0-5号均可见，原始设置0
os.environ['CUDA_VISIBLE_DEVICES'] = '0' 
#警告设置，忽略
warnings.filterwarnings("ignore")

#数据集路径
# dataset_dir = f'{directory}/data/small2019Dataset'
dataset_dir= f'{directory}/ISIC2019datasets'
# GroundTruth CSV 路径 没用到
gd_dir = f'{directory}/ISIC_2019_Training_GroundTruth.csv'

#数据标签平滑，影响训练结果。损失函数
class LabelSmoothSoftmaxCE(nn.Module):
    def __init__(self,
                #  lb_pos=0.96,#超参数 loss要考虑unk类别
                 lb_pos=0.965,#超参数
                 lb_neg=0.005, ##  lb_neg=0.005,#超参数
                 reduction='mean',
                 lb_ignore=255, #这个值貌似不在0-8范围即可。貌似就是为了做平滑。可以设置成为4nv，用来抵消数据量大的影响？
                 ):
        super(LabelSmoothSoftmaxCE, self).__init__()
        self.lb_pos = lb_pos
        self.lb_neg = lb_neg
        self.reduction = reduction
        self.lb_ignore = lb_ignore
        self.log_softmax = nn.LogSoftmax(1)
        # self.softmax = nn.Softmax()
    #logits和label实际对应的是output和target
    def forward(self, logits, label):
        logs = self.log_softmax(logits) #softmax之后取log值,(batch_size)行8列，每行取得softmax
        #按照现在的代码，一个[1,3,5,7,0,3,4,5,6,1] 一维数组是有batchsize决定的。这个和255比肯定都是false呀
        ignore = label.data.cpu() == self.lb_ignore #两个数组比较，对应位置相等取1，不等取0
        n_valid = (ignore == 0).sum() #不相等的数量汇总
        # label[ignore] = 0
        #将标签按照固定格式输出
        lb_one_hot = logits.data.clone().zero_().scatter_(1, label.unsqueeze(1), 1)
        #0.965*标签 +0.005*(1-标签) 标签平滑
        label = self.lb_pos * lb_one_hot + self.lb_neg * (1-lb_one_hot)
        ignore = ignore.nonzero()#返回非零数值在元数组中的下标位置数组
        _, M = ignore.size() #2维数组的化，M是列数
        a, *b = ignore.chunk(M, dim=1) #将数组M拆分成dim=1单位的数组
        label[[a, torch.arange(label.size(1)), *b]] = 0
        if self.reduction == 'mean':
          #平滑 平均 交叉熵
          loss = -torch.sum(torch.sum(logs*label, dim=1)) / n_valid  #按行求和 比上 批处理尺寸
        elif self.reduction == 'none':
          loss = -torch.sum(logs*label, dim=1)  #按行求和交叉熵函数，还是汇总的
        return loss



 
# 需要分类的数目
num_classes = 8
# 批处理尺寸
batch_size =32
# 训练多少个epoch
EPOCH = 30


# 超参数设置 定义已经遍历数据集的次数，加载自己训练的结果时可以根据实际调整
pre_epoch = 0  

'''
 batch中每个元素形如(data, label),过滤数据
'''
def my_collate_fn(batch):
    # 
    batch = list(filter(lambda x: x[0] is not None, batch))
    if len(batch) == 0: return torch.Tensor()
    return default_collate(batch)  # 用默认方式拼接过滤后的batch数据

input_size = 300
#网络加载
net = EfficientNet.from_pretrained('efficientnet-b3')
#True为提取特征值，False则为微调。
feature_extract = False 
# # 要这样修改 FC 输出导出才能是正确维度
feature = net._fc.in_features
net._fc = nn.Linear(in_features=feature,out_features=num_classes,bias=True)
#这行代码的意思是将所有最开始读取数据时的tensor变量copy一份到device所指定的GPU上去，之后的运算都在GPU上进行。
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
if torch.cuda.device_count() > 1: #gpu数量大于1
  net = nn.DataParallel(net)
net = net.to(device)

  # net = nn.DataParallel(net,device_ids=[0,1,2,3,4,5]) #返回值是一个DataParallel，原始的net保存在DataParallel的module变量里面。

# #历史训练结果加载
# checkpoint = torch.load(f'{directory}/RegNet/EfficientNetb1/net_009.pth')
# net.load_state_dict(checkpoint['net'])
# 数据预处理部分
data_transforms = {
    'train': transforms.Compose([
        transforms.Resize(input_size),
        transforms.CenterCrop(input_size),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),

    ]),

    'val': transforms.Compose([
        transforms.Resize(input_size),
        transforms.CenterCrop(input_size),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ]),
}

# Create training and validation datasets
image_datasets = {x: datasets.ImageFolder(os.path.join(dataset_dir, x), data_transforms[x]) for x in ['train', 'val']}
# Create training and validation dataloaders
dataloaders_dict = {
    x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size, shuffle=True, num_workers=12,
                                   collate_fn=my_collate_fn) for x in ['train', 'val']}

 
# b = image_datasets['train'].class_to_idx  #{'AK': 0, 'BCC': 1, 'BKL': 2, 'DF': 3, 'MEL': 4, 'NV': 5, 'SCC': 6, 'VASC': 7}
a = image_datasets['train'].classes   #应该就是上面只有文件夹类别显示
print(a)
#训练中pth文件数据写入地址设置
parser = argparse.ArgumentParser(description='PyTorch DeepNetwork Training')
parser.add_argument('--outf', default=f'{directory}/data/initTxt/', help='folder to output images and model checkpoints')  # 输出结果保存路径
args = parser.parse_args(args=[])

params_to_update = net.parameters() #初始化
print("Params to learn:") #
if feature_extract:#训练更新的情况
    params_to_update = []
    for name, param in net.named_parameters(): #迭代参数名字和参数值
        if param.requires_grad == True:
            params_to_update.append(param)  #将参数的值加入params_to_update，后续优化迭代使用
            print("\t", name)       #打印网路结构，参数名字
else:#微调的情况
    for name, param in net.named_parameters():
        if param.requires_grad == True:
            print("\t", name)

def main():
    ii = 0
    LR = 0.001  # 学习率
    best_acc = 0  # 初始化best test accuracy
    print("Start Training, DeepNetwork!")  # 定义遍历数据集的次数

    # criterion  损失函数的设置
    # criterion=nn.CrossEntropyLoss()
    criterion = LabelSmoothSoftmaxCE()
    # optimizer  优化器  可以尝试不同优化器结合，如RAdam和LookAhead ，第一个参数为优化迭代参数，另外，可以针对不同参数设置不同学习率！
    optimizer = optim.Adam(params_to_update, lr=LR, betas=(0.9, 0.999), eps=1e-9)   
    #加载之前的训练结果，在scheduler之前加载
    # optimizer.load_state_dict(checkpoint['optimizer'])
    # pre_epoch = checkpoint['epoch'] + 1
    # scheduler 学习率的调整策略:ReduceLROnPlateau自适应学习率调整：某指标不再变化（下降或升高），调整学习率，这是非常实用的学习率调整策略。
    scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode='max', factor=0.7, patience=3, verbose=True)


    with open(f'{directory}/data/initTxt/acc.txt', "w") as f:
      with open(f'{directory}/data/initTxt/log.txt', "w")as f2:
        for epoch in range(pre_epoch, EPOCH):
            #一次循环计算一次准确率，就在批循环开始进行初始化
            correctlis = list(0. for i in range(num_classes))
            totallis = list(0. for i in range(num_classes))
            # scheduler.step(epoch) #对进行动态调整
            print('\nEpoch: %d' % (epoch + 1))
            net.train()   #训练
            sum_loss = 0.0
            correct = 0.0
            total = 0.0
            for i, data in enumerate(dataloaders_dict['train'], 0):
                # 准备数据
                length = len(dataloaders_dict['train'])  
                #这里的input和target 就是image和lables的意思
                input, target = data  #target.data是按照一批10个显示每一个的类别
                input.requires_grad=True
                input, target = input.to(device), target.to(device) #将数据加载到gpu中
                # 训练，梯度下降法 forward + backward
                optimizer.zero_grad() #把梯度置零，也就是把loss关于weight的导数变成0
                output = net(input) #网络输出
                #这里要针对outputs计算，超过0.5的才被认为是对应类别，这里也会影响loss的计算
                loss = criterion(output, target)
                loss.backward()
                optimizer.step() #模型更新
                # 每训练1个batch打印一次loss和准确率
                sum_loss += loss.item()  #损失函数累加
                # 这里如果最大值大于0.5就取列，否则取8（unk）
                _, predicted = torch.max(output.data, 1)  #网络输出向量取列最大，为什么是列向量最大？ max取得是具体值 ，predicted是一维10个整数，范围大概是0-7（大概是和8类别对应）
                for j_ in range(len(_)):
                  if _[j_] < 0.5:
                    predicted[j_] = 8
                total += target.size(0) #这个数应该是十
                correct += predicted.eq(target.data).cpu().sum() #eq是两个张量比较，即predicted和target.data比较，相同1不同0 ，使用cpu计算并汇总。理论上式统计正确的个数
                print('[epoch:%d, iter:%d] Loss: %.03f | Acc: %.3f%% '
                      % (epoch + 1, (i + 1 + epoch * length), sum_loss / (i + 1),
                          100. * float(correct) / float(total)))
                f2.write('%03d  %05d |Loss: %.03f | Acc: %.3f%% '
                          % (epoch + 1, (i + 1 + epoch * length), sum_loss / (i + 1),
                            100. * float(correct) / float(total)))
                f2.write('\n')
                f2.flush()
            # 每训练完一个epoch测试val一下准确率
            print("Waiting Test!")
            with torch.no_grad():#val，暂时不追踪网络参数中的导数的目的。减少可能存在的计算和内存消耗。
                correct = 0
                total = 0
                val_loss = 0
                for j, data in enumerate(dataloaders_dict['val'],0): #这个也是按照n批次进行加载
                    net.eval()
                    images, labels = data
                    images, labels = images.to(device), labels.to(device)
                    if j==0:
                      y_true=labels
                    else :
                      y_true = torch.cat((y_true,labels),0)
                    outputs = net(images)
                    
                    # 取得分最高的那个类 (outputs.data的索引号)
                    _, predicted = torch.max(outputs.data, 1)
                    for j_ in range(len(_)):
                      if _[j_] < 0.5:
                        predicted[j_] = 8
                    if j==0:
                      y_val=predicted
                    else :
                      y_val = torch.cat((y_val,predicted),0)
                    total += labels.size(0)
                    loss = criterion(outputs, labels)
                    val_loss += loss.item()
                    # 分类统计recall
                    prediction = torch.argmax(output.data, 1)  #为什么取列向量最大？argmax取得是位置
                    res = predicted == labels   #判断预测和真实值是否一致                   
                    for label_idx in range(len(labels)):     #如果选取的数据中没有这一类的数据，就是0了           
                      if(labels.data[label_idx]==predicted[label_idx]):
                        correctlis[labels.data[label_idx].item()] += res[label_idx].item() #为什么会出现0的情况？
                      totallis[labels.data[label_idx].item()] += 1
                    acc_str = 'Accuracy: %f'%(sum(correctlis)/sum(totallis))
                    correct += (predicted == labels).cpu().sum()

                print('测试分类准确率为：%.3f |loss: %.03f%%' % (100. * float(correct) / float(total), val_loss / (j+1)))
                acc = 100. * float(correct) / float(total)
                scheduler.step(acc)
                for acc_idx in range(8):
                          try:
                              accCount = correctlis[acc_idx]/totallis[acc_idx]
                          except:
                              accCount = 0
                          finally:
                              acc_str += '\tclassID:%d\tacc:%f\t'%(acc_idx, accCount)
                print(acc_str)
                
                #将val标签和验证数据结果保存，用于计算评估数据
                f4 = open(f'{directory}/data/initTxt/'+str(epoch)+'-grouneTrue.txt', "w")
                f5 = open(f'{directory}/data/initTxt/'+str(epoch)+'-predicted.txt', "w")
                for jj in range(len(y_true)):
                    f4.write(str(jj+1)+" "+'{:d}'.format(y_true.data[jj]))
                    f4.write('\n')
                    f5.write(str(jj+1)+" "+'{:d}'.format(y_val.data[jj]))
                    f5.write('\n')
                f4.close()
                f5.close()

                # 记录最佳测试分类准确率并写入best_acc.txt文件中
                if acc > best_acc:
                    f3 = open(f'{directory}/data/initTxt/best_acc.txt', "w")
                    f3.write("EPOCH=%d,best_acc= %.3f%%" % (epoch + 1, acc))
                    f3.close()
                    best_acc = acc
            # 每一个epoch保存一次pth模型
            if (ii % 1 == 0):
                print('Saving model......')
                # torch.save(net.state_dict(), '%s/net_%03d.pth' % (args.outf, epoch + 1))#保存模型参数
                state = {'net':net.state_dict(), 'optimizer':optimizer.state_dict(), 'epoch':epoch}
                torch.save(state, '%s/net_%03d.pth' % (args.outf, epoch + 1))
            # f.write("EPOCH=%03d,Accuracy= %.3f%%" % (epoch + 1, acc))
            f.write("EPOCH=%03d,Accuracy= %.3f val的loss结果为：%.3f 分类测试结果：%s%%" % (epoch + 1, acc,val_loss / (j + 1),acc_str))
            f.write('\n')
            f.flush()    

    print("Training Finished, TotalEPOCH=%d" % EPOCH)

if __name__ == "__main__":
    main()