!pip install scikit-plot  

import numpy as np
from sklearn import metrics
import scikitplot as skplt
import matplotlib.pyplot as plt

'''
模型的预测生成相应的label文件，以及真实类标文件，根据文件读取并加载所有label
1、参数说明：
    file_dir：加载的文件地址。
    文件内数据格式：每行包含两列，第一列为编号1,2，...，第二列为预测或实际的类标签名称。两列以空格为分隔符。
    需要生成两个文件，一个是预测，一个是实际类标，必须保证一一对应，个数一致
2、返回值：
    返回文件中每一行的label列表，例如['5','2','0','7']
'''
def getLabelData(file_dir):
  labels = []
  with open(file_dir,'r',encoding="utf-8") as f:
    for i in f.readlines():
        labels.append(i.strip().split(' ')[1])
  return labels
def getLabelRateData(file_dir):
  # labels = np.zeros([5067,8])
  labels = [[0 for t in range(8)]for i in range(5067)]
  label = []
  ii = 0 
  with open(file_dir,'r',encoding="utf-8") as f:
    for i in f.readlines():
      arr=i.strip().split(' ')
      for j in range(len(arr)):
        if j==len(arr)-1:
          break
        # print(arr[j+1])
        labels[ii][j]=float(arr[j+1])
        # print(labels[ii][j])
      ii = ii + 1
  #       label.append(map(float, arr[j+1]))
  #     labels.append(map(float, label))
  #     label = []
  # print(labels)
  return labels


'''
获取所有类标
返回值：label2idx字典，key表示类名称，value表示编号:{'5': 0, '2': 1, '4': 2, '1': 3, '3': 4, '7': 5, '0': 6, '6': 7, '8': 8}
'''
def getLabel2idx(labels):
  label2idx = dict()
  for i in labels:
      if i not in label2idx:
          label2idx[i] = len(label2idx)
  label2idx['8']=8 #将unk类别加入true
  # print(label2idx)
  return label2idx

'''
  生成混淆矩阵
    矩阵横坐标表示实际的类标，纵坐标表示预测的类标
    矩阵的元素(m1,m2)表示类标m1被预测为m2的个数。
    所有元素的数字的和即为测试集样本数，对角线元素和为被预测正确的个数，其余则为预测错误。
    返回值：返回这个矩阵numpy 和 类别数
    {'AK': 0, 'BCC': 1, 'BKL': 2, 'DF': 3, 'MEL': 4, 'NV': 5, 'SCC': 6, 'VASC': 7，‘UNK'：8}
'''
def buildConfusionMatrix(predict_file,true_file):
  true_labels = getLabelData(true_file)#真实值序列
  predict_labels = getLabelData(predict_file) #预测值序列
  label2idx = getLabel2idx(true_labels) #按照0-7的八类统计个数 再加一个8unk
  #创建混淆矩阵，长宽分别是类别数
  confMatrix = np.zeros([len(label2idx),len(label2idx)],dtype=np.int32)
  for i in range(len(true_labels)):
    true_labels_idx = label2idx[true_labels[i]]
    predict_labels_idx = label2idx[predict_labels[i]]
    confMatrix[true_labels_idx][predict_labels_idx] += 1
  return confMatrix,label2idx

'''
获取某类别的tn, fp, fn, tp
'''
def getBasicData(confMatrix,labelidx): 
  TP = confMatrix[labelidx][labelidx]
  FP = confMatrix.sum(axis=0)[labelidx] - TP
  FN = confMatrix.sum(axis=1)[labelidx] - TP
  TN = confMatrix.sum()-TP-FP-FN
  return TN,FP,FN,TP
'''
 得到RUC必要参数:groundtrue 和 对应类别预测概率
    {'AK': 0, 'BCC': 1, 'BKL': 2, 'DF': 3, 'MEL': 4, 'NV': 5, 'SCC': 6, 'VASC': 7，‘UNK'：8}
'''
def getParaForRUC(true_file,predictrate_file):
  true_labels = getLabelData(true_file)#真实值序列
  predictrate_labels = getLabelData(predictrate_file) #预测值概率序列
  return true_labels,predictrate_labels

'''
总召回率（recall/sensitivity敏感度) = TP/(TP+FN) 对角线上所有值除以总数
'''
def calculate_all_prediction(confMatrix):
  total_sum = confMatrix.sum()
  correct_sum = (np.diag(confMatrix)).sum()
  prediction = round(100*float(correct_sum)/float(total_sum),2)
  return prediction

'''
计算某一个类标的召回率： recall /sensitivity 该类被预测正确的数除以该类的总数;  TP/(TP+FN)  sensitivity
'''
def calculate_label_recall(confMatrix,labelidx):
  label_total_sum = confMatrix.sum(axis=1)[labelidx]
  label_correct_sum = confMatrix[labelidx][labelidx]
  recall = 0
  if label_total_sum != 0:
    recall = round(100*float(label_correct_sum)/float(label_total_sum),2)
  return recall



'''
AUC:ROC曲线下方的面积大小,如下是用预测概率和预测结果两种方式
'''
# def auc(truth_array, prediction_array,typeNum): 
#   a_float = []
#   # for num in prediction_array:
#   #   a_float.append(float(num))
#   # trueth = []
#   for i in range(len(truth_array)):
#     if truth_array[i] == typeNum :
#       trueth.append(1)
#     else:
#       trueth.append(0)
#       #这里还是要全部类别的概率，
#       a_float[i]=0.0001
  
#   print(trueth)
#   print(a_float)
#   auc = metrics.roc_auc_score(trueth, a_float)
#   return auc
def auc(truth_array, prediction_array,typeNum): 
  a_float = []
  trueth = []
  for i in range(len(truth_array)):
    if truth_array[i] == typeNum :
      trueth.append(1)
    else:
      trueth.append(0)
  for i in range(len(prediction_array)):
    if prediction_array[i] == typeNum :
      a_float.append(1)
    else:
      a_float.append(0)
  auc = metrics.roc_auc_score(trueth, a_float)
  return auc
# '''
# mean average precision
# '''
# def average_precision(truth_probabilities, prediction_probabilities):
#   ap = sklearn.metrics.average_precision_score(truth_probabilities, prediction_probabilities)
#   return ap

'''
 TNR  即specificity  特异性  TNR= TN / (FP + TN)  负类被预测为负类的比例
'''
def calculate_label_tnr(TN,FP): 
 if TN + FP == 0:
    return 1.0
 else:
    return TN / (TN+ FP)
'''
TPR sensitivity  敏感度 TPR=TP/ (TP+ FN)  
'''
def calculate_label_tpr(TP,FN): 
 if TP + FN == 0:
    return 1.0
 else:
    return TP / (TP+ FN)

'''
ppv
'''
def calculate_label_ppv(TP,FP): 
 if TP + FP == 0:
    return 1.0
 else:
    return TP / (TP+ FP)

'''
npv
'''
def calculate_label_npv(TN,FN): 
 if TN + FN == 0:
   return 1.0
 else:
   return TN / (TN + FN)

'''
roc
'''
def roc(trues,prerates):
  trues = getLabelData(trues)
  prerate = getLabelRateData(prerates)
  trues = list(map(int, trues))
  # prerate = list(map(float, prerate))
  # trues = [1,2,3]
  # pre =  [[0.599999,0.000001,0.4],[0.1,0.8,0.1],[0.2,0.1,0.7]]
  # print("what is wrong")
  # # skplt.metrics.plot_roc_curve(trues, pre)
  skplt.metrics.plot_roc(trues, prerate)
  plt.show()
# '''
# jaccard
# '''
# def calculate_label_jaccard(confMatrix,labelidx): 

#   return jaccard
# '''
# threshold_jaccard
# '''
# def calculate_label_threshold_jaccard(confMatrix,labelidx): 

#   return threshold_jaccard
# '''
# dice
# '''
# def calculate_label_dice(confMatrix,labelidx): 

#   return dice
# def auc_above_sensitivity()：
#   return sensi

# def balanced_multiclass_accuracy():
#   return bacc
'''
计算某一个类标预测准确率：改类被预测正确为对应类别+不是改类的判定为非该类的数量之和除以判定总数:accuracy
'''
def calculate_label_accuracy(confMatrix,labelidx):
  total_sum = confMatrix.sum()
  #总数减去标签所在行和列，加上二倍标签即为TP+TN
  label_correct_sum = total_sum - confMatrix[labelidx].sum() - confMatrix[:,labelidx].sum() + 2 * confMatrix[labelidx][labelidx]
  acc = 0
  if total_sum != 0:
    acc = round(100*float(label_correct_sum)/float(total_sum),2)
  return acc


'''
Macro F1 score
'''
def calculate_f1(prediction,recall):
  if(prediction+recall)==0:
    return 0
  return round(2*prediction*recall/(prediction+recall),2)

def main():
    ''' predictedrateL2   predictedL2
    该为主函数，可将该函数导入自己项目模块中
    打印精度、召回率、F1值的格式可自行设计
#     '''
filegroundtrue = f'{directory}/models/results/grouneTrue.txt'
filepre = f'{directory}/models/results/predictedL2.txt'
fileprerate = f'{directory}/models/results/predictedrateL22.txt'
filegroundtrue = f'{directory}/models/results/03grouneTrue.txt'
filepre = f'{directory}/models/results/03predictedL1.txt'
fileprerate = f'{directory}/models/results/03predictedrate.txt'
# filegroundtrue = f'{directory}/data/differentopt/test/03grouneTrue.txt'
# filepre = f'{directory}/data/differentopt/test/03predictedL1.txt'
# fileprerate = f'{directory}/data/differentopt/test/03predictedrate.txt'
#     #读取文件并转化为混淆矩阵,并返回label2idx
sortsN = 9
confMatrix,label2idx = buildConfusionMatrix(filepre,filegroundtrue)
total_sum = confMatrix.sum() #样本总个数
# all_prediction = calculate_all_prediction(confMatrix)
true_labels,predictrate_labels = getParaForRUC(filegroundtrue,filepre)
label_auc = []
label_recall = []
label_accuracy = []
# label_mAP = []
label_PPV = []
label_NPV = []
label_TNR = []
label_TPR = []
print('total_sum=',total_sum,',label_num=',len(label2idx),'\n')
# for i in label2idx:
#     print('  ',i)
print('  ')
print('     DF   NV   SCC   BCC   MEL   BKL   AK   VASC   UNK\n')
clas={0:'AK', 1:'BCC', 2:'BKL', 3:'DF', 4:'MEL', 5:'NV', 6:'SCC', 7:'VASC' ,8:'UNK'}
# clas={0:'AK', 1:'BCC', 2:'BKL', 3:'DF', 4:'MEL', 5:'NV', 6:'SCC', 7:'VASC' }
for i in label2idx:
    print(clas.get(int(i)),end=' ')
    label_recall.append(calculate_label_recall(confMatrix,label2idx[i]))
    label_accuracy.append(calculate_label_accuracy(confMatrix,label2idx[i]))
    TN,FP,FN,TP=getBasicData(confMatrix,label2idx[i])
    label_PPV.append(calculate_label_ppv(TP,FP))
    label_NPV.append(calculate_label_npv(TN,FN))
    label_TNR.append(calculate_label_tnr(TN,FP))
    label_TPR.append(calculate_label_tpr(TP,FN))
    if i!='8':
      label_auc.append(auc(true_labels,predictrate_labels,i))
    else :
      label_auc.append(0)
    # 如下是打印混淆矩阵
    for j in label2idx:
        labelidx_i = label2idx[i]
        label2idx_j = label2idx[j]
        print('  ',confMatrix[labelidx_i][label2idx_j],end=' ')
    print('\n')

# print('总召回率（recall）=',all_prediction,'%')
print('individual result :\n')
#分类数据打印  精度  召回率  f1
for ei,i in enumerate(label2idx):
  f1 = calculate_f1(label_PPV[ei]*100,label_recall[ei])
  print(ei,' ',i,' ','accuracy=',label_accuracy[ei],
        '%, recall（TPR）=',label_recall[ei],'%, f1=',f1,
        '%, AUC=',label_auc[ei],', PPV（Precision）=',label_PPV[ei],', NPV=',label_NPV[ei],
        ', TNR（specificity）=',label_TNR[ei])
# print(len(label_prediction))
#总数据 精度  召回率  f1
p = round(np.array(label_PPV).sum()*100/sortsN,2)
r = round(np.array(label_recall).sum()/sortsN,2)
print('MACRO-averaged:\precision=',p,'%,recall=',r,'%,f1=',calculate_f1(p,r),'%')
roc(filegroundtrue,fileprerate)