# 模型导出
import torch 
from torch import nn
from efficientnet_pytorch import EfficientNet

net = EfficientNet.from_pretrained('efficientnet-b5')
# 要这样修改 FC 输出导出才能是正确维度
feature = net._fc.in_features
net._fc = nn.Linear(in_features=feature,out_features=8,bias=True)

checkpoint = torch.load(f'{directory}/data/txt/net_001.pth')
net.load_state_dict(checkpoint['net'])
net.set_swish(memory_efficient=False)
dummy_input = torch.randn(1, 3, 456, 456)
torch.onnx.export(net, dummy_input, "model-b5.onnx", input_names=['input'], output_names=['output'], verbose=True)