# 下载并解压文件
# if os.path.exists(f'{directory}/ISIC_2019_Training_Input.zip'):
#     os.remove(f'{directory}/ISIC_2019_Training_Input.zip')

# !wget "https://s3.amazonaws.com/isic-challenge-2019/ISIC_2019_Training_Metadata.csv"
# !wget "https://s3.amazonaws.com/isic-challenge-2019/ISIC_2019_Training_GroundTruth.csv"
# os.chdir('/content')
# !wget "https://s3.amazonaws.com/isic-challenge-2019/ISIC_2019_Training_Input.zip"
!unzip ISIC_2019_Training_Input.zip -d "/content/drive/My Drive/Colab Notebooks/ISIC2019/wahaha"
import os, random, shutil
import numpy as np
import csv
'''
生成train val  test数据文件夹
'''
def creatDir():
	save_dir=f'{directory}/data/small2019Dataset/'
	for dd in ("train","val","test"):
	  d1=save_dir+dd
	  os.mkdir(d1)
	  for dir in ("/VASC" , "/NV" , "/MEL" , "/DF",  "/BKL" ,"/BCC" ,"/AKIEC","/SCC","/AK"):
		d2=d1+dir
		os.mkdir(d2)
	# save_dir=f'{directory}/train'
	# shutil.rmtree(save_dir)

'''
根据数据集数据读取标签和数据，并对数集进行文件夹分组
'''
def distributionVariData():
	gd_dir = f'{directory}/ISIC_2019_Training_GroundTruth.csv'
	# 读取txt文件并将其转化为array
	f = open(gd_dir,'r')
	reader = csv.DictReader(f)
	data_list = []
	for row in reader:
	  if float(row['VASC'])==1.0:
		data_list.append([row['image'],'VASC'])
		
	  if float(row['NV'])==1.0:
		data_list.append([row['image'],'NV'])
	  
	  if float(row['MEL'])==1.0:
		data_list.append([row['image'],'MEL'])
	  
	  if float(row['BKL'])==1.0:
		data_list.append([row['image'],'BKL'])
	  
	  if float(row['DF'])==1.0:
		data_list.append([row['image'],'DF'])
	  
	  if float(row['BCC'])==1.0:
		data_list.append([row['image'],'BCC'])
	  
	  if float(row['AK'])==1.0:
		data_list.append([row['image'],'AK'])

	  if float(row['SCC'])==1.0:
		data_list.append([row['image'],'SCC'])
	  if float(row['UNK'])==1.0:
		data_list.append([row['image'],'UNK'])
	f.close()
	data_array = np.array(data_list)
	dataset_dir = f'{directory}/wahaha/ISIC_2019_Training_Input'
	dataset_diraim=f'{directory}'
	# 读取每张图片按照其分类复制到相应的文件夹中
	imgs = os.listdir(dataset_dir)
	imgnum = len(imgs)  # 文件夹中图片的数量
	print(imgnum)
	for j in range(len(data_array)):
	  qq=data_array[j]
	  if qq[1]=="AK" or qq[1]=="SCC":
		print(qq[1])
		if os.path.exists(dataset_dir+'/'+qq[0]+'.jpg'):
		  shutil.copy(dataset_dir+'/'+qq[0]+'.jpg', dataset_diraim+'/train/'+qq[1])
		  
'''
分数据
'''
def moveFile(oriDir,aimDir,rate):
  pathDir = os.listdir(oriDir)  # 取图片的原始路径
  filenumber = len(pathDir)
  picknumber = int(filenumber * rate)  # 按照rate比例从文件夹中取一定数量图片
  sample = random.sample(pathDir, picknumber)  # 随机选取picknumber数量的样本图片
  for name in sample:
    print(name)
    # shutil.move(os.path.join(oriDir,name),aimDir)
    shutil.copy(os.path.join(oriDir,name),aimDir)
 
if __name__ == '__main__':
	creatDir()
	distributionVariData()

    tarDir = f'{directory}/data/small2019Dataset/train/'   # 移动到新的文件夹路径
    ori_path = f'{directory}/data/2019datasets/train/' # 最开始train的文件夹路径
    folds=['VASC','BCC','SCC','AK','DF','BKL','MEL','NV']
    for fol in folds:
      oriDir = ori_path+fol # 原图片文件夹路径
      aimDir = tarDir+fol
      moveFile(oriDir,aimDir,0.4)