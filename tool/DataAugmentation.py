'''
放大并保持原图大小
'''
import cv2

def cv2_clipped_zoom(img, zoom_factor):
    """
    Center zoom in/out of the given image and returning an enlarged/shrinked view of 
    the image without changing dimensions
    Args:
        img : Image array
        zoom_factor : amount of zoom as a ratio (0 to Inf)
    """
    height, width = img.shape[:2] # It's also the final desired shape
    new_height, new_width = int(height * zoom_factor), int(width * zoom_factor)

    ### Crop only the part that will remain in the result (more efficient)
    # Centered bbox of the final desired size in resized (larger/smaller) image coordinates
    y1, x1 = max(0, new_height - height) // 2, max(0, new_width - width) // 2
    y2, x2 = y1 + height, x1 + width
    bbox = np.array([y1,x1,y2,x2])
    # Map back to original image coordinates
    bbox = (bbox / zoom_factor).astype(np.int)
    y1, x1, y2, x2 = bbox
    cropped_img = img[y1:y2, x1:x2]

    # Handle padding when downscaling
    resize_height, resize_width = min(new_height, height), min(new_width, width)
    pad_height1, pad_width1 = (height - resize_height) // 2, (width - resize_width) //2
    pad_height2, pad_width2 = (height - resize_height) - pad_height1, (width - resize_width) - pad_width1
    pad_spec = [(pad_height1, pad_height2), (pad_width1, pad_width2)] + [(0,0)] * (img.ndim - 2)

    result = cv2.resize(cropped_img, (resize_width, resize_height))
    result = np.pad(result, pad_spec, mode='constant')
    assert result.shape[0] == height and result.shape[1] == width
    return result

'''
找到有黑边的图,并循环放大。直到没有明显黑边为止
'''
# 获取图的四角像素值，判断四角像素值是不是黑色。
# 是黑色，则放大1.1倍处理。再循环查找
import cv2 
from skimage import io
from PIL import Image
import numpy as np 
import os
def te(img):
  if img[0] == img[1] == img[2]:
    return 0
  return 1                 
# rootdir = f'{directory}/EfficientNet/small2019dataset/train/MEL'
# for parent, dirnames, filenames in os.walk(rootdir):
#   for name in filenames:
    # if(name.find('jpg')!=-1):
    #   img = io.imread(rootdir+'/'+name)
    #   height = img.shape[0]        #将tuple中的元素取出，赋值给height，width，channels
    #   width = img.shape[1]
    #   arr=te(img[0,0])+te(img[height-1,width-1])+te(img[0,width-1])+te(img[height-1,0])
    #   cc=0
    #   while arr<=1:
    #     cc=1
    #     img = cv2_clipped_zoom(img, 1.1)#随机缩放
    #     arr=te(img[0,0])+te(img[height-1,width-1])+te(img[0,width-1])+te(img[height-1,0])
    #   if cc==1:
    #     io.imsave(rootdir+'/oo'+name,img)
name = f'{directory}/EfficientNet/data/small2019Dataset/train/MEL/t_ISIC_0053828.jpg'
rootdir = f'{directory}/EfficientNet/data/small2019Dataset/train/MEL'
# if(name.find('jpg')!=-1):
img = io.imread(name)
height = img.shape[0]        #将tuple中的元素取出，赋值给height，width，channels
width = img.shape[1]
arr=te(img[0,0])+te(img[height-1,width-1])+te(img[0,width-1])+te(img[height-1,0])
cc=0
while arr<=1:
  cc=1
  img = cv2_clipped_zoom(img, 1.1)#随机缩放
  arr=te(img[0,0])+te(img[height-1,width-1])+te(img[0,width-1])+te(img[height-1,0])
if cc==1:
  io.imsave(rootdir+'/oo.jpg',img)

'''
数据增强：水平和竖直镜像 旋转
'''
#coding=utf-8
from PIL import Image
import os
import os.path
import cv2  
import math
import numpy as np 
import matplotlib.pyplot as plt
from skimage import util
from skimage import io
%matplotlib inline 
#图片的水平和竖直翻转  50倍  48倍
i=0
rootdir = f'{directory}/EfficientNet/ISIC2019/train/VASC'  # 指明被遍历的文件夹
for parent, dirnames, filenames in os.walk(rootdir):
    for filename in filenames:
        currentPath = os.path.join(parent, filename)
        # im = Image.open(currentPath)
        # out1 = im.transpose(Image.FLIP_LEFT_RIGHT)
        # newname1 = rootdir+ '/L_' + filename
        # out1.save(newname1)
        # img = io.imread(currentPath)
        # img = cv2_clipped_zoom(img, 1.1)#随机缩放
        # io.imsave(rootdir+'/f'+filename,img)
        # out2 = im.transpose(Image.FLIP_TOP_BOTTOM)
        # newname2 = rootdir + '/t_' + filename
        # out2.save(newname2)
        # # 旋转
        # for degree in (90,180,270):
        #   rotated = im.rotate(degree, Image.BICUBIC, True)  #BICUBIC：双立方滤波。在输入图像的4*4矩阵上进行立方插值
        #   rotated.save(rootdir+'/'+str(degree)+'+'+filename)
        
        for degree in range(5,20,5):
          image = Image.open(currentPath)
          rotated = image.rotate(degree, Image.BICUBIC, True)
          W=image.size[0]
          H=image.size[1]
          angle = math.fabs(degree%180) * math.pi / 180  #计算旋转的角度
          hh=(H*math.cos(angle)-W*math.sin(angle))/(math.pow(math.cos(angle),2)-math.pow(math.sin(angle),2))
          ww=W-hh*math.sin(angle)/math.cos(angle)
          imgcut = rotated.crop(
              (
                rotated.size[0]/2 - ww/2,
                rotated.size[1]/2 - hh/2,
                rotated.size[0]/2 + ww/2,
                rotated.size[1]/2 + hh/2
              )
          )
          imgcut.save(rootdir+'/'+str(degree)+'-'+filename)
     
       